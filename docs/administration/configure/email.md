# Email

Mobilizon requires a SMTP server to deliver emails. Using 3rd-party mail providers (Mandrill, SendGrid, Mailjet, …) will be possible in the future.

## SMTP configuration

Mobilizon default settings assumes a SMTP server listens on `localhost`, port `25`. To specify a specific server and credentials, you can add the following section in your config file and modify credentials to your needs.

```elixir
config :mobilizon, Mobilizon.Web.Email.Mailer,
  adapter: Swoosh.Adapters.SMTP,
  relay: "localhost",
  # usually 25 (clear), 465 (TLS) or 587 (STARTTLS)
  port: 25,
  username: "",
  password: "",
  # can be `:always` or `:never`
  tls: :if_available,
  # not needed when using STARTTLS
  ssl: true,
  allowed_tls_versions: [:"tlsv1.2", :"tlsv1.3"],
  tls_options: [
    verify: :verify_peer,
    versions: [:"tlsv1.2", :"tlsv1.3"],
    cacerts: :public_key.cacerts_get(),
    server_name_indication: "localhost",
    depth: 99
  ],
  retries: 2,
  # can be `true`
  no_mx_lookups: false,
  # can be `:always`. If your smtp relay requires authentication set it to `:always`.
  auth: :if_available
```

You'll need to restart Mobilizon to apply the new settings.

To define from which email address emails are being sent and what address should be used as `Reply-To`, see the `:email_from` and `:email_reply_to` configuration keys in [the configuration reference](reference.md#instance).