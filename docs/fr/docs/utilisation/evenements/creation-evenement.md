# Comment créer un événement

Créons un nouvel événement !

Pour ce faire, vous devez cliquer sur le bouton **Créer** dans la barre de navigation supérieure. Ensuite, vous devez remplir quelques informations pour rassembler les gens&nbsp;!

!!! note
    A chaque instant vous pouvez sauvegarder votre brouillon en cliquant sur le bouton **Enregistrer le brouillon**.
    ![barre fixe pour sauvegarder un brouillon](../../images/create-event-save-FR.png)

## Informations générales

=== "Vide"
    ![événement vide](../../images/create-event-general-information-empty-FR.png)

=== "Avec informations"
    ![événement remplis](../../images/anniversaire-iris-affichage-backend-FR.png)

Dans cette section vous avez la possibilité d'ajouter&nbsp;:

  * une **Image à la une** en cliquant sur le bouton **Cliquez pour uploader**
  * un **Titre** (requis)
  * quelques tags (maximum 10) pour trouver facilement votre événement parmi d'autres similaires (les tags sont ajoutées en appuyant sur la touche `Entrée` ou en les séparant par des virgules)
  * une date de début et une de fin. Vous pouvez aussi choisir d'afficher ou non l'heure de début et/ou de fin en cliquant sur le lien **Paramètres de date**
  * une adresse : vous pouvez soit entrer l'adresse directement, soit demander à être localisé en cliquant sur l'icône <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M12,11.5A2.5,2.5 0 0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5,2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12,2Z" /></svg>. Une fois ajoutée, une carte utilisant OpenStreetMap sera affichée
  * une **description**
  * l'adresse d'un site web

## Organisateur⋅ices

Cette section vous permet de choisir un profil ou un groupe en tant qu'organisateur. Si vous choisissez un groupe, vous pouvez choisir un ou plusieurs contact(s) parmi le groupe.

## Métadonnées de l'événement

Lorsque vous créez un événement, vous avez la possibilité d'ajouter des métadonnées. Vous pouvez ainsi préciser des informations qui seront davantage visibles car elles apparaissent dans la colonne droite de l'événement (sous le champ du site web).

![img qui montre le champ metadonnées](../../images/add-metadata-FR.png)

Ce champ est répétable afin que vous puissiez mettre en évidence plusieurs informations. Vous pouvez préciser le niveau d'accessibilité d'un événement (si l'événement est accessible en fauteuil roulant ou si le direct vidéo de l'événement est interprété en langue des signes), ou bien indiquer un compte sur le fediverse à suivre pour les mises à jour de l'événement, ou même renseigner l'URL d'un live (PeerTube/YouTube/Twitch).

Si le type d'information que vous souhaitez ajouter n'apparaît pas dans nos suggestions, vous pouvez le créer directement. Pour cela, il suffit de cliquer sur le bouton "Ajouter un nouvel élément". Une fenêtre s'affiche et vous propose d'indiquer le titre de l'élément que vous souhaitez ajouter et sa valeur (l'information en elle-même).

![img qui montre une page d'événement avec la colonne métadonnées](../../images/event-metadata-FR.png)

## Qui peut voir cet évènement et y participer

=== "Sans limite de places"
    ![img de qui peut voir et participer à un événement](../../images/create-event-who-FR.png)

=== "Avec nombre de places limité"
    ![img de qui peut voir et participer à un événement avec limite](../../images/create-event-who-limitied-FR.png)

C'est ici que vous pouvez décider qui peut voir et participer à votre événement !

  * **Visilbe partout sur le web  (public)**&nbsp;: n'importe qui peut voir votre événement
  * **Uniquement accessible par lien (privé)**&nbsp;: il faut avoir le lien de votre événement pour le voir (il n'est pas trouvable par une recherche)

**Participations anonymes**&nbsp;: à activer si vous souhaitez autoriser les participations anonymes.

!!! note
    Les participant⋅e⋅s anonymes devront confirmer leur participation par mail.

**Validation des participations**&nbsp;: à cocher si vous souhaitez pouvoir approuver chaque demande de participation (vous pouvez paramétrer leurs notifications [dans votre profil](../utilisateur et utilisatrice/parametres-compte.md#notifications-pour-organisateurice))

**Nombre de places**&nbsp;: si vous souhaitez en limiter le nombre sur votre événement.

## Modération des commentaires publics

![image moderation](../../images/create-event-comment-moderation-FR.png)

Cette section vous permet d'autoriser ou non les commentaires sur la page de votre événement.

## Statut

![événement statut options](../../images/create-event-status-FR.png)

Cette section vous permet de définir si votre événement est&nbsp;:

  * **Provisoire**&nbsp;: sera confirmé plus tard
  * **Confirmé**&nbsp;: aura lieu (par défaut)
  * **Annulé**&nbsp;: n'aura pas lieu
