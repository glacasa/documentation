# Comment créer un profil

Mobilizon vous donne la possibilité de vous gérer plusieurs identités en utilisant plusieurs profils. Pour en créer un, vous devez&nbsp;:

  1. aller dans vos paramètres (en cliquant sur votre avatar puis sur **Mon compte**)
  * cliquer sur **Nouveau profil** dans le menu latéral gauche
  * cliquer sur le bouton **Cliquez pour téléverser** pour ajouter un image depuis votre appareil
  * indiquer un **Nom affiché** (requis)
  * indiquer un **identifiant** (requis) : cet identifiant est unique à votre profil. Il permet à d'autres personnes de vous trouver.
  * remplir une **courte biographie** (facultatif) : pour en dire plus vous concernant :)
  * cliquer sur le bouton **Enregistrer**.

![image de la création du profil de Rose Boreal](../../images/rose-boreal-settings-filled-FR.png)

Désormais, vous pouvez participer à un événement, ou en créer un, avec l'une ou l'autre identité.

!!! note
    Vous pouvez basculer entre vos différentes identités à partir de la barre supérieure, en cliquant sur votre avatar. Par défaut, celle qui s'affiche dans la barre latérale supérieure est celle qui est sélectionnée pour vos actions.

!!! note
    Vous devez rafraîchir la page pour voir votre nouvelle image de profil.
