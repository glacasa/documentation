# Group page

## Join a group

If the group is [configured to allow anyone to join the group without invitation](./create-group.md#settings), you can click the **Join Group** button on the group page. If you are not logged in to your account, you will be prompted to do so.

## Receive group public news

It is possible to follow the public news of a group by using:

   * RSS∕Atom feeds to be used with readers (web/applications) 
   * ICS/WebCal to be used with calendars supporting these formats. 
   
To do this, on the group page you can:
  
  1. click on **⋅⋅⋅**
  * right-click on **RSS/Atom feed** or **ICS/Webcal feed**
  * click on **Copy Link Address** to copy the address to paste.

![image showing news feed menu](../../images/en/rss-group-EN.png)

## Report a group

To report a group:

  1. click the ⋅⋅⋅ button
  * click the **Report** button:

    ![report group image](../../images/en/report-group.png)

  * [Optional but **recommended**:] fill out the report modal with a comment:
    ![group report modal image](../../images/en/report-group-modal.png)
